FROM centos:centos7.6.1810

RUN yum update -y && yum install -y gcc-c++ make \
 && curl -sL https://rpm.nodesource.com/setup_12.x | bash - \
 && yum install -y nodejs \
 && npm install -g nexe